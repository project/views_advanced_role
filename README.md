## Description

This lightweight module will add to functionlity for views access control.

While the default views access control using roles will allow multiple
roles to be specified, access will be granted if the user account has any
of the specified roles.  This is an "OR" option.

The views_advanced_role module will also provide thie same functionality,
but it is extended to provide an option to grant access only if the user
account has all of the specified roles.  This is the "AND" option.

In addtion, the module provide a "NEGATE" option.  This means that access
will be denied if the user has any of the specifed roles (the "OR" option),
or denied if the user has all of the specified roles (the "AND") option.

Expressing the NEGATE options in terms of granting access, the user account
will be granted access if it does not have any of the specified roles (the
"OR" option), or granted access if it does not have all the specified
options (the "AND" option).

## Usage
1. Install the module in the usual way.
2. When configuring a view, selct the "Advanced role access" when
   configuring access restrictions.
3. Select the set of multiple roles using the checkboxes.
4. Select either the "OR" or the "AND" option for combining the roles
5. Select whether to NEGATE the access check or not.


James Scott (jlscott)
7 Sep 2022
