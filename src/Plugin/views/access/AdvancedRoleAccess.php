<?php

namespace Drupal\views_advanced_role\Plugin\views\access;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\Plugin\views\access\Role;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Class ViewsCustomAccess
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *     id = "AdvancedRoleAccess",
 *     title = @Translation("Advanced role access"),
 *     help = @Translation("Add ability to negate selected role(s) access() method"),
 * )
 */
class AdvancedRoleAccess extends Role {

  /**
   * Determine if the current user has access or not.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user who wants to access this view.
   *
   * @return bool
   *   Returns whether the user has access to the view.
   */
  public function access(AccountInterface $account) {
    if ($this->options['conjunction']) {
      // conjunction = "and".
      if ($this->options['negate']) {
        return !empty(array_diff(array_filter($this->options['role']), $account->getRoles()));
      }
      else {
        return empty(array_diff(array_filter($this->options['role']), $account->getRoles()));
      }
    }
    else {
      // conjunction = "or".
      if ($this->options['negate']) {
        return empty(array_intersect(array_filter($this->options['role']), $account->getRoles()));
      }
      else {
        return !empty(array_intersect(array_filter($this->options['role']), $account->getRoles()));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alterRouteDefinition(Route $route) {
    // Specify a custom route access check.
    $route->setRequirement('_views_advanced_role_access_check', 'TRUE');
    // Pass the check options to the check method.
    $route->setRequirement('var_options', serialize($this->options));
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    $title = parent::summaryTitle();
    $text = $title instanceof TranslatableMarkup ? $title->getUntranslatedString() : $title;
    if (count($this->options['role']) > 1) {
      $text .= $this->options['conjunction'] ? ' - All' : ' - Any';
    }
    if ($this->options['negate']) {
      $text .= ' (negated)';
    }
    return $this->t($text);
  }


  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    parent::buildOptionsForm($form, $form_state);

    $options = [
      0 => $this->t('"or": If "negate is not selected, access is allowed if the user has any of the specified role(s). If "negate" is selected, then access is denied if the user has any of the specified role(s).'),
      1 => $this->t('"and": If "negate is not selected, access is allowed if the user has all of the specified role(s). If "negate" is selected, then the user is denied if they have all of the specified role(s).'),
    ];

    $form['conjunction'] = [
      '#type' => 'radios',
      '#title' => $this->t('Conjunction'),
      '#options' => $options,
      '#default_value' => $this->options['conjunction'] ?? 0,
      '#description' => $this->t('Specify how the roles will be joined when more than one is selected.'),
    ];

    $form['negate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Negate the action of the selected role(s).'),
      '#default_value' => $this->options['negate'],
    ];

  }

}
