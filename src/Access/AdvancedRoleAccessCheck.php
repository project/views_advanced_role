<?php

namespace Drupal\views_advanced_role\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\Routing\Route;

/**
 * Checks access for displaying views using the advanced role plugin.
 */
class AdvancedRoleAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route for which an access check is being done.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route) {

    $options = unserialize($route->getRequirement('var_options'));
    $account_roles = $account->getRoles();

    if ($options['conjunction']) {
      // Conjunction = "and".
      $difference = array_diff($options['role'], $account_roles);
      if ($options['negate']) {
        return empty($difference) ? AccessResult::forbidden() : AccessResult::allowed();
      }
      else {
        return empty($difference) ? AccessResult::allowed() : AccessResult::forbidden();
      }
    }
    else {
      // Conjunction = "or".
      $intersect = array_intersect($options['role'], $account_roles);
      if ($options['negate']) {
        return empty($intersect) ? AccessResult::allowed() : AccessResult::forbidden();
      }
      else {
        return empty($intersect) ? AccessResult::forbidden() : AccessResult::allowed();
      }
    }

  }

}
